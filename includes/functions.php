<?php
$names = array (
"username" => "نام کاربری",
"password" => "رمزعبور",
"subject"  => "دسته",
"public"   => "نوع نمایش",
"subject_name"  => "نام دسته",
"group_name"   => "نام گروه",
"repassword" => "تکرار رمزعبور"
);
$error = array();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function direct_to($location) {
  header("Location: " . $location);
  exit();
}
check_page () ;
function check_page () {
  $page_self = basename($_SERVER["PHP_SELF"], ".php");
  if ($page_self === "index" ||
      $page_self === "frize" ||
      $page_self === "login" ||
      $page_self === "show_gallery" ||
      $page_self === "register"
      ) {
      if ($_SESSION["user_type"] !== "public") :
        $goto = "admin.php";
  direct_to($goto);
      endif;
    } else if ($page_self === "manage_users" ||
               $page_self === "new_user" ||
               $page_self === "edit_user" ||
               $page_self === "delete_user") {
      if ($_SESSION["user_type"] !== "admin") :
        $goto = "admin.php";
  direct_to($goto);
      endif;
    } else {
      if ($_SESSION["user_type"] !== "user" &&
          $_SESSION["user_type"] !== "admin") :
        $goto = "index.php";
  direct_to($goto);
      endif;
  }
}

function answer_to_comment($array, $id) {
  $out = "به <span class=\"strong\">";
  foreach ($array as $answer_to) {
    if ($answer_to["id"] === $id) {
      $out .= $answer_to["username"];
    }
  }
 $out .= "</span> پاسخ داده: ";
 return $out;
}

function password_check($password, $hash_password) {
  $hash = crypt($password, $hash_password);
  if ($hash === $hash_password) {
    return true;
  } else {
    return false;
  }
}

function generate_salt($length){
  $unique_random_string = md5(uniqid(mt_rand(), true));
  $base64_string = base64_encode($unique_random_string);
  $modified_base64_string = str_replace("+", ".", $base64_string);
  $salt = substr($modified_base64_string, 0, $length);
  return $salt;
}

function password_encrypt($password, $salt_length = 22){
  $hash_format = "$2y$10$";
  $salt = generate_salt($salt_length);
  $format_and_salt = $hash_format . $salt;
  $hash = crypt($password, $format_and_salt);
  return $hash;
}

function moveElement (&$array, $a, $b) {
  $out = array_splice($array, $a, 1);
  $j = $b + 1;
  $k = true;
  while ($k == true) {
    if ($array[$j]["answer_to"] === $out[0]["answer_to"]) {
//        if ($array[$j]["id"] > $out[0]["id"] && $array[$j]["answer_to"] < $out[0]["answer_to"]) {
//          $j ++;
//        } else {
//          $j --;
//        }
      $j++;
    } else {$k = false;}
  }
  array_splice($array, $j, 0, $out);
//  return $j;
}

function show_comments ($comments) {
  $array = [];
  while ($comment = mysqli_fetch_assoc($comments)) :
    array_push($array, $comment);
  endwhile;
  for ($j = 0; $j < count($array); $j ++) {
    for ($i = 0; $i < count($array); $i ++) {
      if ($array[$j]["id"] == $array[$i]["answer_to"]) :
//        echo $array[$j]["id"] . " <==> " . $array[$i]["answer_to"] . "<br>";
        moveElement($array, $i, $j);
      endif;
    }
  }
  return $array;
}
function find_chosen_name() {
  global $connection;
  $query = "SELECT name, image ";
  $query .= "FROM profiles ";
  $query .= "WHERE username='{$_SESSION['user_name']}' ";
  $query .= "ORDER BY username LIMIT 1";
  $result = mysqli_query($connection, $query);
  if ($result){
    $name = mysqli_fetch_assoc($result);
    $_SESSION['user_image'] = $name["image"];
    $_SESSION['chosen_name'] = $name["name"];
  }
}
find_chosen_name();
function find_from_table($table_and_order, $wheres = NULL, $select = NULL) {
  
  list($table, $order) = $table_and_order;
  if (empty($table) || !isset($table)) return false;
  global $connection;
  if (empty($select) || !isset($select))
    $query  = "SELECT * ";
  else $query = "SELECT {$select} ";
  $query .= "FROM {$table} ";
  for($i = 0;$i < count($wheres); $i ++) :
    if ($i == 0)
      $query .= "WHERE ";
    else 
      $query .= "AND ";
    $query .= "{$wheres[$i]} ";
  endfor;
  $query .= "ORDER BY ";
  if (empty($order) || !isset($order)) {
    $query .= " id ASC;";
  } else {
    $query .= " {$order};";
  }
  $result = mysqli_query($connection, $query);
  confirm_query($result);
//  echo $query;
  return $result;
}

function find_from_gallery($wheres = null) {
  global $connection;
  $query  = "SELECT * ";
  $query .= "FROM gallery ";
  for($i = 0;$i < count($wheres); $i ++) :
    if ($i == 0)
      $query .= "WHERE ";
    else 
      $query .= "AND ";
    $query .= "{$wheres[$i]} ";
  endfor;
  $query .= "ORDER BY rate DESC;";
  $result = mysqli_query($connection, $query);
  confirm_query($result);
  return $result;
}

function message() {
  if ($_SESSION["message"] !== null)
    echo "<div class=\"message\">{$_SESSION['message']}</div>";
  $_SESSION["message"] = null;
}

function show_error() {
  global $error;
  if (empty($error) || !isset($error)) {
    $error = $_SESSION["error"];
  }
  if (!empty($error)){
    echo "<ul class=\"error label-name\">";
    foreach ($error as $value) {
      echo "<li>{$value}</li>";
    }
    echo "</ul>";
  }
    $_SESSION["error"] = null;
}

function validate_edit_form_file($uploaded_file) {
  global $error;
  List($target_file, $file) = $uploaded_file;
  $target_dir = "images/upload/";
  $imagefiletype = pathinfo($_FILES[$file]["name"], PATHINFO_EXTENSION);
//  $target_file = $target_dir . basename($_FILES[$file]["name"]);
  $target_file = $target_dir . "image-{$_SESSION['user_name']}-" . date("Y-n-j-H-i-s") . str_replace(" ", "-", microtime()) . "." . $imagefiletype;
  $uploadok = 1;
  if ($_FILES["file_uploaded"]["size"] > 0) {
    $check = getimagesize($_FILES[$file]["tmp_name"]);
  } else { $check = false; }
  if ($check !== false) {
//    array_push($error, "فایل انتخابی تصویر است. " . $check["mime"]);
    $uploadok = 1;
  } else {
    array_push($error, "file is not an image.");
    $uploadok = 0;
  }
  
  // check if file already exists
  if (file_exists($target_file)) {
    array_push($error, "Sorry, file already exists.");
    $uploadok = 0;
  }
  
  // check file size
  if ($_FILES["file_uploaded"]["size"] > 1000000) {
    array_push($error, "your file is to large.");
    $uploadok = 0;
  }
  
  // allow certain file formats
  if ($imagefiletype != "jpg" &&
      $imagefiletype != "png" &&
      $imagefiletype != "jpeg" &&
      $imagefiletype != "gif") {
    array_push($error, "only jpg, jpeg, png AND gif files are ALLOWED.");
    $uploadok = 0;
  }
  return array($uploadok, $target_file);
}

function validate_form_file($file) {
  global $error;
  $target_dir = "images/upload/";
  $imagefiletype = pathinfo($_FILES[$file]["name"], PATHINFO_EXTENSION);
//  $target_file = $target_dir . basename($_FILES[$file]["name"]);
  $target_file = $target_dir . "image-{$_SESSION['user_name']}-" . date("Y-n-j-H-i-s") . str_replace(" ", "-", microtime()) . "." . $imagefiletype;
  $uploadok = 1;
  if ($_FILES[$file]["size"] > 0) {
  $check = getimagesize($_FILES[$file]["tmp_name"]);
  } else $check = false;
  if ($check !== false) {
//    array_push($error, "فایل انتخابی تصویر است. " . $check["mime"]);
    $uploadok = 1;
  } else {
    array_push($error, "فایل ارسالی تصویر نیست.");
    $uploadok = 0;
  }
  
  // check if file already exists
  if (file_exists($target_file)) {
    array_push($error, "فایل ارسالی از قبل در سایت وجود دارد، لطفا دوباره برای ارسال تلاش نمایید.");
    $uploadok = 0;
  }
  
  // check file size
  if ($_FILES[$file]["size"] > 1000000) {
    array_push($error, "فایل شما باید حجمی کمتر از 1 مگابایت داشته باشد.");
    $uploadok = 0;
  }
  
  // allow certain file formats
  if ($imagefiletype != "jpg" &&
      $imagefiletype != "png" &&
      $imagefiletype != "jpeg" &&
      $imagefiletype != "gif") {
    array_push($error, "تنها فرمتهای  jpg، jpeg، png و gif را می توانید بارگیری نمایید.");
    $uploadok = 0;
  }
  return array($uploadok, $target_file);
}

function validate_form_post($posts) {
  global $names;
  $return = array();
  foreach ($posts as $post) :
    if (!isset($_POST[$post]) || $_POST[$post] === "") :
      if (isset($names[$post])) {
        $return_name = $names[$post];
      } else {
        $return_name = $post;
      }
      array_push($return, "\"" . $return_name . "\" نمی تونه خالی باشه.");
    endif;
  endforeach;
  return $return;
}

function find_all_subjects() {
  global $connection;
  $query = "SELECT * ";
  $query .= "FROM subjects ";
  $query .= "ORDER BY title ASC;";
  $result = mysqli_query($connection, $query);
  confirm_query($result);
  return $result;
}
function confirm_query($result) {
    if (!$result) die(mysqli_connect_error());
}

function find_all_groups () {
  global $connection;
  $query = "SELECT * ";
  $query .= "FROM groups ";
  $query .= "ORDER BY name ASC;";
  $result = mysqli_query($connection, $query);
  confirm_query($result);
  return $result;
}
function find_subjects_by_group($id) {
  global $connection;
  $query = "SELECT * ";
  $query .= "FROM subjects ";
  $query .= "WHERE group_id = {$id} ";
  $query .= "ORDER BY title ASC;";
  $result = mysqli_query($connection, $query);
  confirm_query($result);
  return $result;
}
function sidebar() {
  if ($_SESSION["user_type"] === "public") {
    global $connection;
    $result_group = find_all_groups();
    while ($group_row = mysqli_fetch_assoc($result_group)) :
    $check = find_from_table(["subjects", "group_id ASC LIMIT 1"], ["group_id = {$group_row['id']}"], "id");
    if (mysqli_num_rows($check) > 0) {
      echo "<h3>{$group_row['name']}</h3>";
    }
    mysqli_free_result($check);
    echo "<ul>";
    $result_subjects = find_subjects_by_group($group_row["id"]);
    while ($subject_row = mysqli_fetch_assoc($result_subjects)) :
      echo "<li><a href=\"index.php?gallery={$subject_row['id']}\">{$subject_row['title']}</a></li>";
    endwhile;
    echo "</ul>";
    mysqli_free_result($result_subjects);
    endwhile;
    mysqli_free_result($result_group);
  } else if ($_SESSION["user_type"] === "user") {
    echo "<a href=\"admin.php\"><h3>مدیریت</h3></a>";
    echo "<ul>";
      echo "<li><a href=\"manage_gallery.php\">مدیریت گالری</a></li>";
      echo "<li><a href=\"manage_subjects.php\">مدیریت دسته ها</a></li>";
      echo "<li><a href=\"manage_groups.php\">مدیریت گروه ها</a></li>";
      echo "<li><a href=\"manage_comments.php\">مدیریت نظرات</a></li>";
      echo "<li><a href=\"profile.php\">پروفایل</a></li>";
      echo "<li><a href=\"change_password.php\">تغییر رمزعبور</a></li>";
    echo "</ul>";
  } else if ($_SESSION["user_type"] === "admin") {
    echo "<a href=\"admin.php\"><h3>مدیریت</h3></a>";
    echo "<ul>";
      echo "<li><a href=\"manage_gallery.php\">مدیریت گالری</a></li>";
      echo "<li><a href=\"manage_subjects.php\">مدیریت دسته ها</a></li>";
      echo "<li><a href=\"manage_groups.php\">مدیریت گروه ها</a></li>";
      echo "<li><a href=\"manage_users.php\">مدیریت کاربران</a></li>";
      echo "<li><a href=\"manage_comments.php\">مدیریت نظرات</a></li>";
      echo "<li><a href=\"profile.php\">پروفایل</a></li>";
      echo "<li><a href=\"change_password.php\">تغییر رمزعبور</a></li>";
    echo "</ul>";
  }
}