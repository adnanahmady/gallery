<!DOCTYPE html>
<html>
  <head>
    <title>Great Gallery</title>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, minimum-scale=0.5, maximum-scale=3">
    <link rel="stylesheet" type="text/css" href="stylesheets/reset.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/style.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/font-awesome.min.css">
    <link rel="stylesheet/less" type="text/css" href="stylesheets/style.less">
    <script src="javascripts/jquery-3.2.1.min.js"></script>
    <script src="javascripts/less.2.7.2.min.js"></script>
    <script src="javascripts/js.js"></script>
    <?php if (basename($_SERVER["PHP_SELF"], ".php") == "manage_subjects") { ?>
    <script src="javascripts/jquery.js"></script>
    <?php } else { echo '<script src="javascripts/p_jquery.js"></script>'; } ?>
  </head>
  <body>
    <header id="page-header">
      <span id="user-box">
        <?php
        if ($_SESSION["user_type"] === "public") {
        ?>
          <a href="login.php" title="login">ورود</a>
          <a href="register.php" title="register">ثبت نام</a>
        <?php 
        } else { ?>
          <label><?php echo ($_SESSION["chosen_name"] !== null ? $_SESSION["chosen_name"] : $_SESSION["user_name"]); ?> عزیز خوش آمدید.</label>
          <a href="logout.php" title="logout">خروج</a>
        <?php
        }
        ?>
      </span>
      <img src="<?php echo (isset($_SESSION["user_image"]) ? $_SESSION["user_image"] : "images/e.jpg" ); ?>" alt="<?php echo $_SERVER["HTTP_HOST"]; ?>" id="site-icon">
    </header>
    <div id="page-container">