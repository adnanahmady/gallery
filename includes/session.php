<?php
session_start();

if (!isset($_SESSION["user_name"])) {
  $_SESSION["user_name"] = "anonymouse";
}
if (!isset($_SESSION["message"]))
  $_SESSION["message"] = null;
if (!isset($_SESSION["user_type"]))
  $_SESSION["user_type"] = "public";
if (!isset($_SESSION["error"])) {
  $_SESSION["error"] = null;
}
