<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] === "public") :
  direct_to("index.php");
endif;

if (!isset($_POST["submit"])) {
  direct_to("login.php");
}
$command = $_POST["submit"];

switch ($command) {
  case "edit_subject":
    edit_subject($_POST["subject_name"], $_POST["group_name"], $_POST["id"]);
    break;
  case "delete_subject":
    delete_subject($_POST["id"]);
    break;
  default: 
    direct_to("login.php");
    break;
}

function edit_subject($subject, $group, $id) {
  global $connection;
  $error = validate_form_post(["subject_name", "group_name", "id"]);
  if (empty($error)) {
    $query  = "SELECT * FROM subjects ";
    $query .= "WHERE title = '{$subject}' ";
    $query .= "AND group_id = (SELECT id FROM groups WHERE name = '{$group}');";
    $result = mysqli_query($connection, $query);
    $query = "SELECT id FROM groups WHERE name = '{$group}' ORDER BY name LIMIT 1;";
    $group_result = mysqli_query($connection, $query);
    if (mysqli_num_rows($result) == 0 && mysqli_num_rows($group_result) > 0) {
  //    INSERT INTO subjects (title, group_id, user_owner) VALUES ('bear', (SELECT id FROM groups WHERE name = 'animales'), 'adnan')]
      $group_id = mysqli_fetch_assoc($group_result);
      $query  = "UPDATE subjects ";
      $query .= "SET title='{$subject}', group_id={$group_id['id']} ";
      $query .= "WHERE id = {$id} ";
      $update_subject = mysqli_query($connection, $query);
      if ($update_subject) {
        $_SESSION["message"] = "بروزرسانی فیلد با موفقیت انجام شد.";
        echo "success";
      } else {
        array_push($error, "متاسفانه بروزرسانی با شکست مواجه شد.");
    echo show_error($error);
      }
    } else {
    echo show_error($error);
    }
    $_SESSION["error"] = $error;
    echo show_error($error);
  } else {
    echo show_error($error);
  }
}

function delete_subject($id) {
  global $connection;
  $error = validate_form_post(["id"]);
  if (empty($error)) {
    $query  = "DELETE FROM subjects ";
    $query .= "WHERE id = '{$id}';";
    $result = mysqli_query($connection, $query);
    if ($result) {
      $query  = "DELETE FROM gallery ";
      $query .= "WHERE subject_id = {$id};";
      $delete = mysqli_query($connection, $query);
      if ($delete)
        echo "success";
    }
  } else {
    echo show_error($error);
  }
}

if (isset($connection)) {
  mysqli_close($connection);
}