<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] === "public") :
  direct_to("index.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";

$username = $_SESSION["user_name"];
if (isset($_POST["change"])) :
   $error = validate_form_post(["password", "repassword"]);
   if (empty($error)) :
      $password = filter_input(INPUT_POST, "password", FILTER_DEFAULT);
      $repassword = filter_input(INPUT_POST, "repassword", FILTER_DEFAULT);
      if ($password !== $repassword) :
        array_push($error, "تکرار رمزعبور باید با رمزعبور یکی باشد.");
      endif;
      $new_password = password_encrypt($password);
      $query  = "UPDATE users ";
      $query .= "SET password='{$new_password}' ";
      $query .= "WHERE username = '{$username}';";
      $edits = mysqli_query($connection, $query);
      if ($edits) {
//        $_SESSION["message"] = "{$username} با موفقیت به روز رسانی شد.";
        direct_to("logout.php");
      } else {
        array_push($error, "{$username} قادر به بروزرسانی نیست.");
      }
   endif;
endif;
show_error();
if (isset($_SESSION["user_name"])) :
  $query  = "SELECT * FROM users ";
  $query .= "WHERE username = '{$username}' ";
  $query .= "ORDER BY username ASC LIMIT 1;";
  $selects = mysqli_query($connection, $query);
  if ($selects) :
    $select = mysqli_fetch_assoc($selects);
?>
<form action="change_password.php" method="post" class="is-normal-form">
    <label class="label-name"><?php echo $_SESSION["user_name"]; ?></label>
    <input type="hidden" value="<?php echo $select["username"]; ?>">
    <label for="password">رمزعبور جدید</label><input type="password" name="password">
    <label for="repassword">تکرار رمزعبور</label><input type="password" name="repassword">
    <button type="submit" name="change">تغییر رمزعبور</button>
  </form>
<?php
  endif;
  mysqli_free_result($selects);
endif;
include "../includes/layouts/footer.php";