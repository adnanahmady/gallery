<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] !== "admin") :
  direct_to("admin.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";

if (filter_input(INPUT_GET, "user", FILTER_VALIDATE_URL) !== NULL) :
  $username = filter_input(INPUT_GET, "user", FILTER_DEFAULT);
  $query  = "SELECT username ";
  $query .= "FROM users ";
  $query .= "WHERE username = '{$username}' ";
  $query .= "ORDER BY username LIMIT 1;";
  $select = mysqli_query($connection, $query);
  if (mysqli_num_rows($select) > 0) {
    $query  = "DELETE FROM users ";
    $query .= "WHERE username = '{$username}';";
    $result = mysqli_query($connection, $query);
    if ($query && mysqli_affected_rows($connection)) {
      $_SESSION["message"] = "{$username} با موفقیت حذف شد.";
      direct_to("manage_users.php");
    } else {
      $_SESSION["error"] = mysqli_error($connection) . " (" . mysqli_errno($connection) . ") \r\nAND THE QUERY THAT YOU USED IS: '{$query}'";
      direct_to("manage_users.php");
    }
  } else {
    $_SESSION["error"] = "کاربری با مشخصاتی که شما درخواست دادید وجود ندارد.";
    direct_to("manage_users.php");
  }
endif;
include "../includes/layouts/footer.php";