<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] === "public") :
  direct_to("index.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";

if (!isset($_GET["gallery"]) && !isset($_GET["delete"])) :
  direct_to("manage_gallery.php");
endif;

$query  = "SELECT * ";
$query .= "FROM gallery ";
if (isset($_GET["delete"])) 
  $query .= "WHERE id = {$_GET["delete"]} ";
else 
  $query .= "WHERE id = {$_GET["gallery"]} ";
$query .= "LIMIT 1;";
$result = mysqli_query($connection, $query);
confirm_query($result);
$row = mysqli_fetch_assoc($result);

if (isset($_GET["delete"])) :
  $query  = "DELETE FROM gallery WHERE id = {$_GET['delete']};";
  $result = mysqli_query($connection, $query);
  if ($result) {
    if (file_exists($row["image"])) { unlink($row["image"]); }
    $_SESSION["message"] = "گالری با موفقیت حذف شد.";
    direct_to("manage_gallery.php");
  } else {
    $error = "در پاک کردن گالری مشکلی پیش آمد.";
  }
  
endif;

if (isset($_POST["submit"])) :
  $error = validate_form_post(array("subject", "public"));
  if ($_FILES["file_uploaded"]["name"] != "")
    List($uploadok, $target_file) = validate_edit_form_file([$row["image"], "file_uploaded"]);
  if (empty($error)) :
    $subject = $_POST["subject"];
    $describtion = $_POST["describtion"];
    $public = $_POST["public"];
    $owner = $_SESSION["user_name"];


    $query  = "UPDATE gallery SET ";
    $query .= "subject_id = {$subject}, describtion = '{$describtion}', ";
    // check if $uploadok is set to 0 by an error
    if (isset($uploadok) && $uploadok == 1) {
      if (move_uploaded_file($_FILES["file_uploaded"]["tmp_name"], $target_file)) {
        if (isset($target_file)) :
          if (file_exists($row["image"])) { unlink($row["image"]); }
          $query .= "image = '{$target_file}', ";
        endif;
      }
    }
    $query .= " user_owner = '{$owner}', public = {$public} ";
    $query .= "WHERE id = {$_GET['gallery']};";
    $insert = mysqli_query($connection, $query);
    confirm_query($insert);
    $_SESSION["message"] = "گالری با موفقیت ویرایش شد.";
    direct_to("manage_gallery.php");
  endif;
endif;
show_error();
?>
<form action="edit_gallery.php?gallery=<?php echo $_GET['gallery']; ?>" method="post" enctype="multipart/form-data" class="is-gallery">
  <label class="label-name"><?php echo $_SESSION["user_name"]; ?></label>
  <label class="img"><img src="<?php echo $row['image']; ?>" alt="<?php echo $row['alt']; ?>">
   
  <input type="file" name="file_uploaded" id="file_upload"></label>
  <label>دسته</label>
  <select name="subject">
    <option value="">----</option>
    <?php
    $subjects = find_all_subjects();
    while($subject = mysqli_fetch_assoc($subjects)) :
    ?>
    <option value="<?php echo $subject["id"]; ?>"
    <?php
    if ($row["subject_id"] == $subject["id"]) 
      echo " selected=\"selected\"";
    ?>
            ><?php echo $subject["title"]; ?></option>
    <?php endwhile; ?>
  </select>
  <label for="describtion">توضیح</label>
  <textarea name="describtion" cols="30" rows="10"><?php echo $row["describtion"]; ?></textarea>
  <label for="public">نمایش عمومی</label>
  <select name="public">
    <option value="">----</option>
    <option value="0"<?php
    if ($row["public"] == 0) 
      echo " selected=\"selected\"";
    ?>>خیر</option>
    <option value="1"<?php
    if ($row["public"] == 1) 
      echo " selected=\"selected\"";
    ?>>بله</option>
  </select>
  <input type="submit" class="fiftin" name="submit" value="ویرایش گالری">
  <a class="btn-form fiftin" href="edit_gallery.php?delete=<?php echo $_GET["gallery"]; ?>">حذف گالری</a>
</form>
<?php
mysqli_free_result($subjects);
mysqli_free_result($result);
include "../includes/layouts/footer.php";