<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] !== "admin") :
  direct_to("admin.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";


if (isset($_POST["edit"])) :
  $username = filter_input(INPUT_GET, "user", FILTER_DEFAULT);
  $frize = filter_input(INPUT_POST, "frize", FILTER_VALIDATE_INT);
  $type = filter_input(INPUT_POST, "type", FILTER_VALIDATE_INT);
  $query  = "UPDATE users ";
  $query .= "SET frize={$frize}, TYPE={$type} ";
  $query .= "WHERE username = '{$username}';";
  $edits = mysqli_query($connection, $query);
  if ($edits) {
    $_SESSION["message"] = "{$username} با موفقیت به روز رسانی شد.";
    //$_SESSION["error"] = $query;
//    echo $query;
    direct_to("manage_users.php");
  } else {
    array_push($error, "{$username} قادر به بروزرسانی نیست.");
  }
endif;
show_error();
if (filter_input(INPUT_GET, "user", FILTER_VALIDATE_URL) !== null) :
  $username = filter_input(INPUT_GET, "user", FILTER_DEFAULT);
  $query  = "SELECT * FROM users ";
  $query .= "WHERE username = '{$username}' ";
  $query .= "ORDER BY username ASC LIMIT 1;";
  $selects = mysqli_query($connection, $query);
  if ($selects) :
    $select = mysqli_fetch_assoc($selects);
?>
  <form action="edit_user.php?user=<?php echo $username; ?>" method="post" class="is-profile">
    <label class="label-name"><?php echo $_SESSION["user_name"]; ?></label>
    <label class="user-name"><?php echo $select["username"]; ?></label>
    <label name="frize">انجماد کاربر</label>
    <select name="frize">
      <option value="0"<?php if ($select["frize"] == 0) echo " selected=\"selected\""; ?>>غیر منجمد</option>
      <option value="1"<?php if ($select["frize"] == 1) echo " selected=\"selected\""; ?>>منجمد</option>
    </select>
    <label name="type">سطح کاربر</label>
    <select name="type">
      <option value="0"<?php if ($select["TYPE"] == 0) echo " selected=\"selected\""; ?>>کاربر عادی</option>
      <option value="1"<?php if ($select["TYPE"] == 1) echo " selected=\"selected\""; ?>>کاربر مدیر</option>
    </select>
    <input type="submit" name="edit" value="ویرایش کاربر">
  </form>
<?php
  endif;
  mysqli_free_result($selects);
endif;
include "../includes/layouts/footer.php";