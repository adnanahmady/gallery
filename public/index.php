<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] !== "public") :
  direct_to("admin.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";
?>
        <div class="gallery">
          <?php
//          length of array items
//          $err = array("adnan", "erfan");
//          echo count($err);
          if (isset($_GET['gallery'])) {
            $result = find_from_gallery(["public = 1", "subject_id = {$_GET['gallery']}"]);
          } else {
            $result = find_from_table(["gallery", "id ASC"], ["public = 1"]);
          }
          while ($gallery_row = mysqli_fetch_assoc($result)) :
          ?>
          <a href="show_gallery.php?gallery_id=<?php echo $gallery_row["id"]; ?>" class="link-gallery">
            <div class="gallery-item">
              <img src="<?php echo $gallery_row["image"]; ?>" alt="<?php echo $gallery_row["alt"]; ?>" class="gallery-image">
              <p class="gallery-item-text"><?php
              if (strlen($gallery_row["describtion"]) > 1)
                echo substr($gallery_row["describtion"], 0, 20) . "[...]";
              else 
                echo $gallery_row["describtion"];
  //            echo strlen($gallery_row["describtion"]);
              ?></p>
            </div>
          </a>
          <?php
          endwhile;
          mysqli_free_result($result);
          ?>
        </div>
<?php

include "../includes/layouts/footer.php";