"use strict";
var selected_element = 0;

$(function() {
  
  $(document).on("click", "form table .delete", function() {
    var ask = confirm("با حذف دسته تصاویر آن هم پاک خواهند شد آیا از حذف دسته مطمئن هستید.");
    if (ask === true) {
      $.ajax({url: "ajax_proccess.php", type: "POST",
        data: {
          submit: "delete_subject",
          id: $(this).attr("name")
      },
      success: function (data) {
        if (data == "success") {
          window.location.reload();
        }
      }
      });
    }
  });
  
  $(document).delegate("form table .edit", "click", function() {
    var t = $(this).parent().prevUntil("td:first-child").find("#foo");
    if (t.val() !== undefined) {
      var span = $("<span>").html(t.val());
      t.replaceWith(span);
    }
    var loop = $(this).parent().prevUntil("td:first-child");
    
    $.ajax({url: "ajax_proccess.php", type: "POST",
      data: {
        submit: "edit_subject",
        id: $(this).attr("name"),
        subject_name: loop.eq(1).text(),
        group_name: loop.eq(0).text()
    },
    success: function (data) {
      window.location.reload();
//    alert(data);
//    if (data == "success") {
//      window.location.reload();
//    }
    },
    error: function (status, responseTxt, xhr) {
      alert("xhr.statusText");
    }
  });
//    for (var i = 0; i < loop.length; i ++ ) {
//      alert(loop.eq(i).text());
//    }
  });
  
  $(document).on("click","form table span", function() {
    var t = $("table").find("#foo");
    if (t.val() !== undefined) {
      var span = $("<span>").html(t.val());
      t.replaceWith(span);
    }
    t = $("<input>", {
      id: "foo",
      type: "text",
      value: $(this).text()
    });
    $(this).replaceWith(t);
  });
});