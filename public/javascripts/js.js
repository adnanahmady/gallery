"use strict";
if (window.addEventListener) {
  window.addEventListener("load", function () {
    Doc();
  });
} else {
  window.attachEvent("onload", function () {
    Doc();
  });
}
function Doc() {
  if (document.getElementsByClassName("cansel")[0] !== undefined) {
    document.getElementsByClassName("cansel")[0].addEventListener("click", function() {
      goBack("admin.php");
    });
  }
  if (document.getElementsByClassName("close")[0] !== undefined) {
    document.getElementsByClassName("close")[0].addEventListener("click", function() {
      goBack("index.php");
    });
  }

//  var sidebar = document.getElementById("sidebar").children;
//  alert(sidebar[1].attributes[0]);
//  var i;
//  for (i = 0; i < sidebar.length; i += 2) {
//    sidebar[i].onclick = function () {
//      if (sidebar[i+1].attributes[0] !== undefined) {
//        sidebar[i+1].removeClass("show");
//      } else sidebar[i+1].addClass("show");
////      sidebar[i+1].classList.toggle("show");
//    };
//  }

  document.getElementsByName("subject_id")[0].addEventListener("change", function () {
    var str = this.value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementsByName("position")[0].innerHTML = this.responseText;
        // alert();
      }
    };
    xmlhttp.open("GET", "ajax.php?cmd=find_page&q=" + str, true);
    xmlhttp.send();
  });
  
  // if (document.addEventListener) {
  //   document.getElementsByClassName("close")[0].addEventListener("click", function() {
  //     window.location = "index.php";
  //   });
  // } else {
  //   document.getElementsByClassName("close")[0].attachEvent("onclick", function() {
  //     window.location = "index.php";
  //   });
  // }
}
function goBack(path) {
  if (path === undefined || path === null) {
    path = "index.php";
  }
  
  if (document.referrer == "") {
    location.href=path;
  } else {
    history.back();
  }
}