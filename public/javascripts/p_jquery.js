"use strict";
$(document).ready(function() {
  $(document).on("click", ".changable table span", function() {
    var span, text;
    text = $(this).parents("table").find("#foo");
    if (text.val() !== undefined) {
      span = $("<span>").text(text.val());
      text.replaceWith(span);
    }
    text = $("<input>", {
      id: "foo",
      value: $(this).text(),
      type: "text"
    });
    $(this).replaceWith(text);
  });
  $(document).on("click", "form table .edit-group", function() {
    var text = $("table").find("#foo");
    if (text !== undefined) {
      var span = $("<span>").text(text.val());
      text.replaceWith(span);
    }
    var gets = $(this).parent().prevUntil("td:nth-child(1)").text();
    var link = $(this).attr("name");
    window.location.href = "?edit=" + link + "&group_name=" + gets;
  });
});