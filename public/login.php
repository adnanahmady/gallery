<?php
require_once "../includes/session.php";
require_once "../includes/db_connection.php";
require_once "../includes/functions.php";
if ($_SESSION["user_type"] !== "public") :
  direct_to("admin.php");
endif;

if (isset($_POST["submit"])) :
  $error = validate_form_post(["username", "password"]);
  $username = $_POST["username"];
  $password = $_POST["password"];
  
//  $new_password = password_encrypt($password);
//  echo $new_password;
  $query  = "SELECT * ";
  $query .= "FROM users ";
  $query .= "WHERE username = '{$username}' ";
//  $query .= "AND password = '{$new_password}' ";
  $query .= "ORDER BY username ASC ";
  $query .= "LIMIT 1;";
  $result = mysqli_query($connection, $query);
//  confirm_query($result);
  if ($result) {
    $user = mysqli_fetch_assoc($result);
//  echo $password;
  $check = password_check($password, $user["password"]);
  if ($check) {
    $_SESSION["user_name"] = $user["username"];
    if ($user["TYPE"] == 0) {
    $_SESSION["user_type"] = "user";
    } else if ($user["TYPE"] == 1) {
    $_SESSION["user_type"] = "admin";
    }
    direct_to("admin.php");
  } else {
    array_push($error, "متاسفانه ورود شما با شکست روبرو شد.");
  }
  }
    mysqli_free_result($result);
endif;
?><!DOCTYPE html>
<html>
  <head>
    <title>Great Gallery</title>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, minimum-scale=0.5, maximum-scale=3">
    <link rel="stylesheet" type="text/css" href="stylesheets/reset.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/login.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/font-awesome.min.css">
    <link rel="stylesheet/less" type="text/css" href="stylesheets/style.less">
    <script src="javascripts/less.2.7.2.min.js"></script>
    <script src="javascripts/js.js"></script>
  </head>
  <body>
    <div id="page-container">
      <form action="login.php" method="post">
        <label class="close"></label>
        <label class="label-name">لطفا نام کاربری و رمزعبور خود را با دقت وارد نمایید.</label>
        <label for="username">نام کاربری</label><input type="text" name="username" tab-index="0">
        <label for="password">رمزعبور</label><input type="password" name="password" tab-index="1">
        <input type="submit" name="submit" value="ورود به حساب کاربری">
        <label class="label-name"><?php
        if (!isset($error) || $error == null) 
          echo "خطاها در این قسمت نشان داده خواهند شد.";
        else
          show_error();
        ?>
        </label>
      </form>
    </div>
    <footer id="page-footer"><p><sup>&copy;</sup>تمامی حقوق این سایت متعلع است به عدنان احمدی در سال <?php echo date("Y"); ?></p></footer>
  </body>
</html>

<?php

if (isset($connection)) {
  mysqli_close($connection);
}