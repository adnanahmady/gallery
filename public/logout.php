<?php
include "../includes/session.php";
include "../includes/functions.php";
$_SESSION["user_name"] = null;
$_SESSION["chosen_name"] = null;
$_SESSION["user_type"] = "public";
session_destroy();
direct_to("login.php");