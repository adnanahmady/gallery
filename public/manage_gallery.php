<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] === "public") :
  direct_to("index.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";
message();
?>
          <a class="btn" href="new_gallery.php">ایجاد گالری جدید</a>
        <div class="gallery">
          <?php
          $result = find_from_gallery(["user_owner = '{$_SESSION['user_name']}'"]);
          while ($gallery_row = mysqli_fetch_assoc($result)) :
          ?>
          <a href="edit_gallery.php?gallery=<?php echo $gallery_row['id']; ?>">
            <div class="gallery-item">
            <?php
            if ($gallery_row["public"] == 1) :
              echo "<i class=\"fa fa-share-alt public\" aria-hidden=\"true\"></i>";
            endif;
            ?>
              <img src="<?php echo $gallery_row["image"]; ?>" alt="<?php echo $gallery_row["alt"]; ?>" class="gallery-image">
              <p class="gallery-item-text"><?php
              if (strlen($gallery_row["describtion"]) > 1)
                echo substr($gallery_row["describtion"], 0, 20) . "...";
              else 
                echo $gallery_row["describtion"];
  //            echo strlen($gallery_row["describtion"]);
              ?></p>
            </div>
          </a>
          <?php
          endwhile;
          mysqli_free_result($result);
          ?>
        </div>
<?php

include "../includes/layouts/footer.php";