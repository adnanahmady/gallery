<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] === "public") :
  direct_to("index.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";
$error = null;
$error = array();

if (isset($_GET["delete"])) {
//  if (empty($error)) {
    $group_id = $_GET["delete"];
    $query  = "SELECT id FROM subjects ";
    $query .= "WHERE group_id = {$group_id};";
    $subjects = mysqli_query($connection, $query);
    confirm_query($subjects);
    while ($subject = mysqli_fetch_assoc($subjects)) :
      $query  = "DELETE FROM gallery ";
      $query .= "WHERE subject_id = {$subject['id']};";
      mysqli_query($connection, $query);
    endwhile;
    
    $query  = "DELETE FROM subjects ";
    $query .= "WHERE group_id = {$group_id};";
    mysqli_query($connection, $query);
    
    $query  = "DELETE FROM groups ";
    $query .= "WHERE id = {$group_id};";
    mysqli_query($connection, $query);
//  }
}
if (isset($_GET["edit"])) :
//  $error = validate_form_post(array("group_name"));
//  if (empty($error)) :
    $group = mysqli_real_escape_string($connection,$_GET["edit"]);
    $group = $_GET["group_name"];
    $user = $_SESSION["user_name"];
    $query  = "UPDATE groups ";
    $query .= "SET name='{$group}', owner='{$user}' ";
    $query .= "WHERE id = {$_GET['edit']};";
//    echo $query;
    $result = mysqli_query($connection, $query);
    if ($result) {
      $_SESSION["message"] = "گروه با موفقیت ویرایش شد.";
      message();
    } else {
      array_push($error, "عملیات شکست خورد.");
    }
//  endif;
endif;
if (isset($_POST["new"])) :
  $error = validate_form_post(array("group_name"));
  if (empty($error)) :
    $group = mysqli_real_escape_string($connection,$_POST["group_name"]);
//    $group = $_POST["group_name"];
    $user = $_SESSION["user_name"];
    $query  = "INSERT INTO groups ";
    $query .= "(name, owner) ";
    $query .= "VALUES ('{$group}', '{$user}');";
//    echo $query;
    $result = mysqli_query($connection, $query);
    if ($result) {
      $_SESSION["message"] = "گروه با موفقیت ایجاد شد.";
      message();
    } else {
      array_push($error, "عملیات شکست خورد.");
    }
  endif;
endif;
//message();
      show_error();
?>

<form action="manage_groups.php" method="post" class="has-table changable">
  <table>
    <tr>
      <th>شماره</th>
      <th>نام گروه</th>
      <th>مدیریت دسته ها</th>
    </tr>
    <?php
//    $subjects = find_from_table(["groups", "title ASC"], ["user_owner = '{$_SESSION['user_name']}'"]);
    $i = 1;
      $group = find_from_table(["groups", "name ASC;"]);
    while ($group_row = mysqli_fetch_assoc($group)) :
    ?>
    <tr>
      <td><?php echo $i++; ?></td>
      <td><span><?php echo $group_row["name"]; ?></span></td>
      <td>
        <i class="fa fa-pencil-square-o edit edit-group" aria-hidden="true" title="ویرایش" name="<?php echo $group_row["id"]; ?>"></i>
        <a href="manage_groups.php?delete=<?php echo $group_row["id"]; ?>"><i class="fa fa-times delete" aria-hidden="true" title="حذف" name="<?php echo $group_row["id"]; ?>"></i></a>
      </td>
    </tr>
    <?php
    endwhile;
      mysqli_free_result($group);
    ?>
    <tr>
      <td>گروه جدید</td>
      <td>
        <input type="text" name="group_name">
      </td>
      <td><input type="submit" name="new" value="گروه جدید"></td>
    </tr>
  </table>
</form>
<?php

include "../includes/layouts/footer.php";