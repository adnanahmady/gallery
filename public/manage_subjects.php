<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] === "public") :
  direct_to("index.php");
endif;
include "../includes/layouts/header.php"; 


if (isset($_POST["submit"])) :
//  unset($error);
  $error = validate_form_post(["subject_name", "group_name"]);
  if (empty($error)) :
    
  $subject = $_POST["subject_name"];
  $group = $_POST["group_name"];
//  $subjects = find_from_table(["subjects", "id ASC"], ["user_owner = '{$_SESSION['user_name']}'"]);
  $query  = "SELECT * FROM subjects ";
  $query .= "WHERE title = '{$subject}' ";
  $query .= "AND group_id = (SELECT id FROM groups WHERE name = '{$group}')";
  $result = mysqli_query($connection, $query);
  confirm_query($result);
  if (mysqli_num_rows($result) == 0) {
//    INSERT INTO subjects (title, group_id, user_owner) VALUES ('bear', (SELECT id FROM groups WHERE name = 'animales'), 'adnan')]
      $query  = "INSERT INTO ";
      $query .= "subjects (";
      $query .= "title, group_id, user_owner";
      $query .= ") VALUES (";
      $query .= "'{$subject}', (";
        $is_group_exists = find_from_table(["groups", "id ASC"], ["name = '{$group}'"]);
        if (mysqli_num_rows($is_group_exists) == 0) {
          $insert_group = "INSERT INTO groups ";
          $insert_group .= "(name, owner) ";
          $insert_group .= "VALUES ('{$group}', '{$_SESSION['user_name']}')";
          $result_insert_group = mysqli_query($connection, $insert_group);
          if (!$result) {
            array_push($error, "ورود اطلاعات با شکست مواجه شد.");
            return false;
          }
        }
      $query .= "SELECT id ";
      $query .= "FROM groups ";
      $query .= "WHERE name = '{$group}'";
      $query .= "), '{$_SESSION['user_name']}');";
      $result = mysqli_query($connection, $query);
      if ($result) {
        $_SESSION["message"] = "دسته {$subject} با موفقیت به گروه {$group} اظافه شد.";
        direct_to($_SERVER["PHP_SELF"]);
      } else {
        array_push($error, "دسته {$subject} نتوانست اضافه شود.");
      }
    
  } else {
    array_push($error, "دسته {$subject} از قبل در گروه {$group} وجود دارد.");
  }
  endif;
//  unset($_POST);
//  $_POST = array();
//  $_POST["submit"] = "no required";
//  echo "<pre>";
//  var_dump($_POST);
//  echo "</pre>";
//  direct_to($_SERVER["PHP_SELF"]);
endif;
include "../includes/layouts/sidebar.php";

show_error();
message();
?>
<p class="demo"></p>
<form action="manage_subjects.php" method="post" class="has-table">
  <table>
    <tr>
      <th>شماره</th>
      <th>نام دسته</th>
      <th>نام گروه</th>
      <th>مدیریت دسته ها</th>
    </tr>
    <?php
    $subjects = find_from_table(["subjects", "title ASC"], ["user_owner = '{$_SESSION['user_name']}'"]);
    $i = 1;
    while ($subject_row = mysqli_fetch_assoc($subjects)) :
    ?>
    <tr>
      <td><?php echo $i++; ?></td>
      <td><span><?php echo $subject_row["title"] ?></span></td>
      <?php 
      $group = find_from_table(["groups", "name ASC LIMIT 1"], ["id = {$subject_row['group_id']}"]);
      if (mysqli_num_rows($group) > 0) {
        $finded_group = mysqli_fetch_assoc($group);
      ?>
      <td><span><?php echo $finded_group["name"]; ?></span></td>
      <?php 
      }
      mysqli_free_result($group);
      ?>
      <td>
        <i class="fa fa-pencil-square-o edit" aria-hidden="true" title="ویرایش" name="<?php echo $subject_row["id"]; ?>"></i>
        <i class="fa fa-times delete" aria-hidden="true" title="حذف" name="<?php echo $subject_row["id"]; ?>"></i>
      </td>
    </tr>
    <?php
    endwhile;
    mysqli_free_result($subjects);
    ?>
    <tr>
      <td>دسته جدید</td>
      <td>
        <input list="subjects" name="subject_name">
        <datalist id="subjects">
          <?php
          $list_subjects = find_from_table(["subjects", "title ASC"], null, "DISTINCT title");
          while ($sl_row = mysqli_fetch_assoc($list_subjects)) :
          ?>
          <option value="<?php echo $sl_row['title']; ?>">
          <?php
          endwhile;
          mysqli_free_result($list_subjects);
          ?>
        </datalist>
      </td>
      <td>
        <input list="groups" name="group_name">
        <datalist id="groups">
          <?php
          $list_groups = find_from_table(["groups", "name ASC"], null, "DISTINCT name");
          while ($sl_row = mysqli_fetch_assoc($list_groups)) :
          ?>
          <option value="<?php echo $sl_row['name']; ?>">
          <?php
          endwhile;
          mysqli_free_result($list_groups);
          ?>
        </datalist>
      </td>
      <td><input type="submit" name="submit" value="دسته جدید"></td>
    </tr>
  </table>
</form>
<?php

include "../includes/layouts/footer.php";