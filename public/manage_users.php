<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] !== "admin") :
  direct_to("admin.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";
message();
?>
<form action="manage_subjects.php" method="post" class="has-table">
  <table>
    <tr>
      <th>شماره</th>
      <th>نام کاربری</th>
      <th>انجماد کاربر</th>
      <th>سطح کاربر</th>
      <th colspan="1">مدیریت کاربر</th>
    </tr>
    <?php
    $users = find_from_table(["users", "username ASC"], null, "username, frize, TYPE");
    $i = 1;
    while ($user = mysqli_fetch_assoc($users)) :
    ?>
    <tr>
      <td><?php echo $i++; ?></td>
      <td><span><?php echo $user["username"] ?></span></td>
      <td><span><?php echo ($user["frize"] == 1 ? "کاربر منجمد شده است" : "کاربر در حالت انجماد نیست"); ?></span></td>
      <td><span><?php echo ($user["TYPE"] == 1 ? "مدیر" : "عادی"); ?></span></td>
      <td>
          <i class="fa fa-pencil-square-o edit" aria-hidden="true" title="ویرایش" name="<?php echo $user["username"]; ?>"></i>
          <i class="fa fa-times delete" aria-hidden="true" title="حذف" name="<?php echo $user["username"]; ?>"></i>
      </td>
    </tr>
    <?php
    endwhile;
    mysqli_free_result($users);
    ?>
    <tr>
      <td colspan="5"><a href="new_user.php" class="btn-form">کاربر جدید</a></td>
    </tr>
  </table>
</form>
<script>
  $(document).on("click", ".edit", function() {
    var val = $(this).attr("name");
    location.href="edit_user.php?user=" + val;
  });
  $(document).on("click", ".delete", function() {
    var val = $(this).attr("name");
    location.href="delete_user.php?user=" + val;
  });
</script>
<?php
include "../includes/layouts/footer.php";