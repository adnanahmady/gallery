<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] === "public") :
  direct_to("index.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";

if (isset($_POST["submit"])) :
//  echo "<pre>" . var_dump($_FILES["file_uploaded"]) . "</pre>";
  $error = validate_form_post(array("subject", "public"));
  List($uploadok, $target_file) = validate_form_file("file_uploaded");
  if (empty($error)) :
    // check if $uploadok is set to 0 by an error
    if ($uploadok == 1) {
      if (move_uploaded_file($_FILES["file_uploaded"]["tmp_name"], $target_file)) {
        $subject = $_POST["subject"];
        $describtion = $_POST["describtion"];
        $public = $_POST["public"];
        $owner = $_SESSION["user_name"];
        
        $query  = "INSERT INTO gallery (";
        $query .= "subject_id, describtion, image, user_owner, public ";
        $query .= ") VALUES (";
        $query .= "{$subject}, '{$describtion}', '{$target_file}', '{$owner}', {$public} ";
        $query .= ");";
        $insert = mysqli_query($connection, $query);
        confirm_query($insert);
        $_SESSION["message"] = "gallery created.";
        direct_to("manage_gallery.php");
      }
    }
  endif;
endif;
show_error();
?>
<form action="new_gallery.php" method="post" enctype="multipart/form-data" class="is-form">
  <label class="label-name"><?php echo $_SESSION["user_name"]; ?></label>
  <label for="file_uploaded">تصویر</label>
  <input type="file" name="file_uploaded" id="file_upload">
  <label for="file_uploaded">دسته</label>
  <select name="subject">
    <option value="">----</option>
    <?php
    $subjects = find_all_subjects();
    while($subject = mysqli_fetch_assoc($subjects)) :
    ?>
    <option value="<?php echo $subject["id"]; ?>"><?php echo $subject["title"]; ?></option>
    <?php endwhile; ?>
  </select>
  <label for="file_uploaded">متن همراه</label>
  <textarea name="describtion" cols="30" rows="10"></textarea>
  <label for="file_uploaded">نوع نمایش</label>
  <select name="public">
    <option value="">----</option>
    <option value="0">خصوصی</option>
    <option value="1">عمومی</option>
  </select>
  <input type="submit" name="submit" value="ایجاد گالری">
</form>
<?php

include "../includes/layouts/footer.php";