<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] !== "admin") :
  direct_to("admin.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";

if (NULL !== filter_input(INPUT_POST, "user", FILTER_DEFAULT)) :
  $error = validate_form_post(["username", "password"]);
  if (empty($error)) {
    $username = filter_input(INPUT_POST, "username", FILTER_DEFAULT);
    $password = filter_input(INPUT_POST, "password", FILTER_DEFAULT);
    $frize = filter_input(INPUT_POST, "frize", FILTER_DEFAULT);
    $type = filter_input(INPUT_POST, "type", FILTER_DEFAULT);
    $query  = "INSERT INTO users (";
    $query .= "username, password, frize, TYPE";
    $query .= ") VALUES (";
    $query .= "'{$username}', '{$password}', {$frize}, {$type});";
    $result = mysqli_query($connection, $query);
    if ($result && mysqli_affected_rows($connection)) {
      $_SESSION["message"] = "{$username} با موفقیت ایجاد شد.";
      direct_to("manage_users.php");
    } else {
      array_push($error, mysqli_error($connection) . " (" . mysqli_errno($connection) . ") <br>\r\nAND THE QUERY IS: ". $query );
    }
  }
endif;
show_error();
?>
<form action="new_user.php" method="post" class="is-form">
  <label class="label-name"><?php echo $_SESSION["user_name"]; ?></label>
  <label for="username">نام کاربر</label>
  <input type="text" name="username" placeholder="نامی برای کاربر جدید مشخص نمایید">
  <label for="password">رمزعبور</label>
  <input type="text" name="password" placeholder="رمزعبوری برای کاربر جدید انتخاب نمایید">
  <label for="frize">انجماد کاربر</label>
  <select name="frize">
    <option value="0">کاربر  منجمد نشود</option>
    <option value="1">کاربر  منجمد شود</option>
  </select>
  <label for="type">سطح کاربر</label>
  <select name="type">
    <option value="0">کاربر عادی</option>
    <option value="1">کاربر مدیر</option>
  </select>
  <input type="submit" name="user" value="ایجاد کاربر جدید">
</form>
<?php

include "../includes/layouts/footer.php";