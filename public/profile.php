<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] === "public") :
  direct_to("index.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";

if (isset($_POST["submit"])) :
//  unset($error);
  if (!empty($_FILES["image"]["name"]))
    list($uploadok, $target_file) = validate_form_file("image");
//  $error = validate_form_post(["name", "describe"]);
  
  if (empty($error)) :
    
  $username = $_POST["name"];
  $describe = $_POST["describe"];
//  $subjects = find_from_table(["subjects", "id ASC"], ["user_owner = '{$_SESSION['user_name']}'"]);
  $query  = "SELECT * FROM profiles ";
  $query .= "WHERE username = '{$_SESSION['user_name']}' LIMIT 1;";
  $result = mysqli_query($connection, $query);
  
  confirm_query($result);
  
  if (mysqli_num_rows($result) == 0) {
//    INSERT INTO subjects (title, group_id, user_owner) VALUES ('bear', (SELECT id FROM groups WHERE name = 'animales'), 'adnan')]
    $query  = "INSERT INTO ";
    $query .= "profiles (";
    $query .= "username, name, image, user_describe";
    $query .= ") VALUES (";
    $query .= "'{$_SESSION['user_name']}', '{$username}',";
    if ($uploadok == 1) {
      // check if $uploadok is set to 0 by an error
      $row = mysqli_fetch_assoc($result);
      if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
        if (isset($target_file)) {
          if (file_exists($row["image"])) { unlink($row["image"]); }
          $query .= "'{$target_file}' ";
        }
      }
    } else {
      $query .= "'' ";
    }
    $query .= ", '{$describe}');";
    $result3 = mysqli_query($connection, $query);
    if ($result3) {
      $_SESSION["message"] = "تبریک پروفایل شما با موفقیت بروزرسانی شد.";
      direct_to($_SERVER["PHP_SELF"]);
    } else {
      array_push($error, "در بروزرسانی پروفایل مشکلی رخ داد");
    }
  } else {
    $query  = "UPDATE profiles SET ";
    $query .= "name='{$username}', ";
    if (isset($uploadok) && $uploadok == 1) {
      // check if $uploadok is set to 0 by an error
      $row = mysqli_fetch_assoc($result);
      if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
        if (isset($target_file)) {
          if (file_exists($row["image"])) { unlink($row["image"]); }
          $query .= "image='{$target_file}' ";
        }
      }
    }
    $query .= ", user_describe='{$describe}' ";
    $query .= "WHERE username='{$_SESSION['user_name']}';";
    $result2 = mysqli_query($connection, $query);
    if ($result2) {
      $_SESSION["message"] = "تبریک پروفایل شما با موفقیت بروزرسانی شد.";
      direct_to($_SERVER["PHP_SELF"]);
    } else {
      array_push($error, "در بروزرسانی پروفایل مشکلی رخ داد");
    }
  }
  endif;
//  unset($_POST);
//  $_POST = array();
//  $_POST["submit"] = "no required";
//  echo "<pre>";
//  var_dump($_POST);
//  echo "</pre>";
//  direct_to($_SERVER["PHP_SELF"]);
endif;
show_error();
message();

$query  = "SELECT * FROM profiles ";
$query .= "WHERE username = '{$_SESSION['user_name']}' LIMIT 1;";
$result = mysqli_query($connection, $query);
confirm_query($result);

$row = mysqli_fetch_assoc($result);
?>
<p class="demo"></p>
<form action="profile.php" method="post" enctype="multipart/form-data" class="is-profile">
  <label class="label-name"><?php echo $_SESSION["user_name"]; ?></label>
  <img src="<?php echo $row["image"]; ?>" alt=""/>
  <label for="image">تصویر شما</label>
  <input type="file" name="image">
  <label for="name">نام انتخابی</label>
  <input type="text" name="name" value="<?php echo $row["name"]; ?>">
  <label for="describe">توضیحات</label>
  <textarea name="describe" cols="30" rows="10"><?php echo $row["user_describe"]; ?></textarea>
  <input type="submit" name="submit" value="ثبت">
  <input type="button" class="cansel" value="انصراف">
</form>
<?php

include "../includes/layouts/footer.php";