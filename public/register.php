<?php
require_once "../includes/session.php";
require_once "../includes/db_connection.php";
require_once "../includes/functions.php";

function cr () {
  $text = 'QWERTYUIOPASDFGHJKLZXCVBNM123456789';
  $code = '';
  for($i = 1; $i <= 6; $i ++) {
    $start = rand(0, strlen($text));
    $code .= substr($text, $start, 1);
  }

  $font_size = 30;
  $image_width = 200;
  $image_height = 60;
  $image = imagecreate($image_width, $image_height);
  imagecolorallocate($image, 255,155,0);
  $text_color = imagecolorallocate($image, 0,0,0);
  imagettftext($image, $font_size, 0, 30, 45, $text_color, "fonts/TAHOMA.ttf", $code);
  imagejpeg($image, "image.jpg");
  $_SESSION["code"] = $code;
}
if (isset($_POST["submit"])) :
  $error = validate_form_post(["username", "password", "repassword", "captcha"]);
    $username = $_POST["username"];
    $password = $_POST["password"];
    $repassword = $_POST["repassword"];
    $captcha = $_POST["captcha"];
    $created_captcha = $_SESSION["code"];
    if ($captcha !== $created_captcha) {
      array_push($error, "لطفا متن امنیتی را درست نمایید.");
    }
    if ($password !== $repassword) {
      array_push($error, "تکرار رمزعبور با رمزعبور تطابق ندارد.");
    }
  if (empty($error)) :
    $new_password = password_encrypt($password);
    $query  = "INSERT INTO users ";
    $query .= "(username, password, frize, TYPE) VALUES ";
    $query .= "('{$username}', '{$new_password}', 0, 0);";
    $result = mysqli_query($connection, $query);
  //  confirm_query($result);
    if ($result) {
//      $_SESSION["message"] = "کاربر با موفقیت اضافه شد.";
      mysqli_free_result($result);
      direct_to("login.php");
    } else {
      array_push($error, "متاسفانه ورود شما با شکست روبرو شد.");
    }
  endif;
endif;
cr();
?><!DOCTYPE html>
<html>
  <head>
    <title>Great Gallery</title>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, minimum-scale=0.5, maximum-scale=3">
    <link rel="stylesheet" type="text/css" href="stylesheets/reset.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/login.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/font-awesome.min.css">
    <link rel="stylesheet/less" type="text/css" href="stylesheets/style.less">
    <script src="javascripts/less.2.7.2.min.js"></script>
    <script src="javascripts/js.js"></script>
  </head>
  <body>
    <div id="page-container">
      <form action="register.php" method="post">
        <label class="close"></label>
        <label class="label-name">لطفا نام کاربری و رمزعبور خود را با دقت وارد نمایید.</label>
        <label for="username">نام کاربری</label><input type="text" name="username" tab-index="0">
        <label for="password">رمزعبور</label><input type="password" name="password" tab-index="1">
        <label for="repassword">رمزعبور</label><input type="password" name="repassword" tab-index="2">
        <img src="image.jpg" class="center">
        <label for="captcha">رمزعبور</label><input type="text" name="captcha" tab-index="2">
        <input type="submit" name="submit" value="ایجاد به حساب کاربری">
        <label class="label-name"><?php
        if (!isset($error) || $error == null) 
          echo "خطاها در این قسمت نشان داده خواهند شد.";
        else
          show_error();
        ?>
        </label>
      </form>
    </div>
    <footer id="page-footer"><p><sup>&copy;</sup>تمامی حقوق این سایت متعلع است به عدنان احمدی در سال <?php echo date("Y"); ?></p></footer>
  </body>
</html>

<?php

if (isset($connection)) {
  mysqli_close($connection);
}