<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
if ($_SESSION["user_type"] === "public") :
  direct_to("index.php");
endif;
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";

if (filter_input(INPUT_POST, "new", FILTER_VALIDATE_URL) !== NULL) :
  $username = $_SESSION["user_name"];
  $image_id = filter_input(INPUT_GET, "gallery", FILTER_DEFAULT);
  $comment = filter_input(INPUT_POST, "comment", FILTER_DEFAULT);
  $query  = "INSERT INTO comments ";
  $query .= "(image_id, username, comment, answer_to) ";
  $query .= "VALUES ({$image_id}, '{$username}', '{$comment}', ";
    $answer_to = (filter_input(INPUT_POST, "answer_to", FILTER_DEFAULT) === null ? "0" : filter_input(INPUT_POST, "answer_to", FILTER_DEFAULT));
    $query .= "{$answer_to});";
  $insert = mysqli_query($connection, $query);
  if ($insert) {
    $_SESSION["message"] = "نظر شما با موفقیت افزوده شد.";
    direct_to($_SERVER["PHP_SELF"] . "?gallery={$image_id}");
message();
  } else {
    $_SESSION["error"] = "مشکلی در افزودن نظر شما رخ داده است.";
    direct_to($_SERVER["PHP_SELF"] . "?gallery={$image_id}");
show_error();
  }
endif;
?>
        <div class="show-gallery">
          <form action="show_comments.php?gallery=<?php echo $_GET["gallery"]; ?>" method="post">
          <?php
//          length of array items
//          $err = array("adnan", "erfan");
//          echo count($err);
          if (isset($_GET['gallery'])) {
            $result = find_from_table(["gallery", "id ASC LIMIT 1"], ["id = {$_GET['gallery']}"]);
            while ($gallery_row = mysqli_fetch_assoc($result)) :
            ?>
                <img src="<?php echo $gallery_row["image"]; ?>" alt="<?php echo $gallery_row["alt"]; ?>" class="gallery-image">
                <p class="gallery-item-text"><?php
                if (strlen($gallery_row["describtion"]) > 1)
                  echo $gallery_row["describtion"];
                ?></p><hr>
            <?php
              $comments = find_from_table(array("comments", "id ASC"), array("image_id = {$_GET['gallery']}"));
              $array = show_comments($comments);
//              print_r($array);
              echo "<div dir=\"ltr\">";
//              foreach ($array as $kay => $comment) {
//                echo $kay . " => " . $comment["id"] . " <==> " . $comment["answer_to"] . "<br>";
//              }
              foreach ($array as $comment) {
                if ($comment["username"] == $_SESSION["user_name"]) {
                  echo "<div class=\"user-comment\">";
                  ?>
                <label class="comment">
                <input type="radio" name="answer_to" value="<?php echo $comment["id"]; ?>">
                <span class="strong"><?php echo $comment["username"]; ?></span>
                <?php 
                $answer_to_this = answer_to_comment($array, $comment["answer_to"]);
                echo $answer_to_this;
                ?>
                <p><?php echo $comment["comment"]; ?></p>
                </label>
                <?php
                  echo "</div>";
                } else {
                  echo "<div class=\"others-comment\">";
                  ?>
                <label class="comment">
                <input type="radio" name="answer_to" value="<?php echo $comment["id"]; ?>">
                <span class="strong"><?php echo $comment["username"]; ?></span>
                <?php 
                $answer_to_this = answer_to_comment($array, $comment["answer_to"]);
                echo $answer_to_this;
                ?>
                <p><?php echo $comment["comment"]; ?></p>
                </label>
                <?php
                  echo "</div>";
                }
//              echo "<span class=\"inline-block\">({$key})</span>[". print_r($value). "]<br>";
              }
              echo "</div>";
              
            endwhile;
            mysqli_free_result($comments);
            mysqli_free_result($result);
          }
          ?>
            <label for="reply"><input type="checkbox" name="reply" value="1">
            به این کاربر پاسخ نده</label>
            <textarea name="comment" cols="30"></textarea>
            <input type="submit" value="new comment" class="normal-submit" name="new">
          </form>
        </div>
<?php
include "../includes/layouts/footer.php";