<?php
include "../includes/session.php";
include "../includes/db_connection.php";
include "../includes/functions.php";
include "../includes/layouts/header.php"; 
include "../includes/layouts/sidebar.php";
?>
        <div class="show-gallery">
          <?php
//          length of array items
//          $err = array("adnan", "erfan");
//          echo count($err);
          if (isset($_GET['gallery_id'])) {
            $result = find_from_table(["gallery", "id ASC LIMIT 1"], ["id = {$_GET['gallery_id']}"]);
            while ($gallery_row = mysqli_fetch_assoc($result)) :
            ?>
                <img src="<?php echo $gallery_row["image"]; ?>" alt="<?php echo $gallery_row["alt"]; ?>" class="gallery-image">
                <p class="gallery-item-text"><?php
                if (strlen($gallery_row["describtion"]) > 1)
                  echo $gallery_row["describtion"];
                ?></p>
            <?php
              $comments = find_from_table(array("comments", "id ASC"), array("image_id = {$_GET['gallery_id']}"));
              $array = show_comments($comments);
              echo "<div dir=\"ltr\">";
              foreach ($array as $comment) {
                if ($comment["username"] == $_SESSION["user_name"]) {
                  echo "<div class=\"user-comment\">";
                  ?>
                <label class="comment">
                <span class="strong"><?php echo $comment["username"]; ?></span>
                <p><?php echo $comment["comment"]; ?></p>
                </label>
                <?php
                  echo "</div>";
                } else {
                  echo "<div class=\"others-comment\">";
                  ?>
                <label class="comment">
                <span class="strong"><?php echo $comment["username"]; ?></span>
                <p><?php echo $comment["comment"]; ?></p>
                </label>
                <?php
                  echo "</div>";
                }
              }
              echo "</div>";
              
            endwhile;
            mysqli_free_result($comments);
            mysqli_free_result($result);
          }
          ?>
        </div>
<?php

include "../includes/layouts/footer.php";